#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <mqueue.h>

#include "calculation.h"
#include "output.h"

/* Output thread function prototype */
void *tOutputThreadFunc(void *);

/* Thread variable */
pthread_t tOutputThread;
/* Thread attributes */
pthread_attr_t aOutputThreadAttr;
/* Output barrier */
pthread_barrier_t output_barrier;

/* Mqueue variable */
mqd_t outputMQueue;
/* Mqueue attributes structure */
struct	mq_attr outputMQueueAttr;

int output_debug_mode = 0;

/* Output thread initialization function */
int init_output() {

	int status;

    /* Set Message Queue size */
	outputMQueueAttr.mq_maxmsg = 2048;
	outputMQueueAttr.mq_msgsize = sizeof(struct output_data_t);

	/* Create Message Queue */
	if ((outputMQueue = mq_open("/outputMQ", O_CREAT | O_RDWR, 777, &outputMQueueAttr)) == -1) {
		fprintf(stderr, "Creation of the mqueue outputMQ failed.\n");
	}

	/* Initialize barrier */
	pthread_barrier_init(&output_barrier, NULL, 2);

	/* Initialize attributes structure */
	pthread_attr_init(&aOutputThreadAttr);

	/* Create output thread */
	if ((status = pthread_create(&tOutputThread, &aOutputThreadAttr, tOutputThreadFunc, NULL))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	return 0;
}

/*
 *  Output thread function
 */
void *tOutputThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_min(policy);
	pthread_setschedparam( pthread_self(), policy, &param );

	/* Receive, identify and replay to message*/
	for (;;) {
		/* Wait until you get pulse */
		pthread_barrier_wait(&output_barrier);

		/* Put the input and output value to the output */
		pthread_mutex_lock(&output_mutex);

		mq_send(outputMQueue, (const char *)&output_data, sizeof(struct output_data_t), 0);

        if (output_debug_mode)
        {
		    printf("vehicle angular velocity: %.4f wheel angular velocity: %.4f, distance: %.1f\n", output_data.vehicle_angular_velocity, output_data.wheel_angular_velocity, output_data.vehicle_distance);
        }

        pthread_mutex_unlock(&output_mutex);
	}

	return 0;
}

/**
 * Finalize output
 */
int finalize_output() {

	/* Close Message Queue */
	mq_close(outputMQueue);

	return 0;
}
