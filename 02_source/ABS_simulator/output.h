#ifndef OUTPUT_H_
#define OUTPUT_H_

/* Output barrier */
extern pthread_barrier_t output_barrier;

/* Atomic flag */
extern int output_debug_mode;

/* Function creates output thread */
int init_output();
/* Function close logger */
int finalize_output();

#endif /* OUTPUT_H_ */
