#include <gtk/gtk.h>
#include <stdlib.h>
#include <mqueue.h>
#include <unistd.h>
#include <signal.h>


GtkWidget * gLblVehicleVelocity;
GtkWidget * gLblVehicleDistance;
GtkWidget * gLblSlip;
GtkWidget * gLblFriction;
GtkWidget * gLblAbsOnOff;
GtkWidget * gLblInitVehicleVelocity;


struct output_data_t {
	double vehicle_velocity;

	double vehicle_angular_velocity;
	double vehicle_distance;

	double wheel_angular_velocity;

	double force;

	double slip;
	double friction;

	double brake_torque;
};

struct input_data_t {
	int abs_on_off;
	double init_vehicle_velocity;
	int reset_simulation;
};


/* PID for ABS_simulator process */
pid_t abs_simulator_pid;

/* Logger thread prototype */
void *tThreadFunc(void *);

/* Mqueue variable */
mqd_t outputMQueue;
/* Mqueue attributes structure */
struct	mq_attr outputMQueueAttr;

/* Mqueue variable */
mqd_t inputMQueue;
/* Mqueue attributes structure */
struct	mq_attr inputMQueueAttr;

/* Thread variable */
pthread_t tShowDataThread;
/* Thread attributes structure */
pthread_attr_t aShowDataThreadAttr;

struct input_data_t inputData = {
    .abs_on_off = 1,
    .init_vehicle_velocity = 27.778,
    .reset_simulation = 0
};

int gui_initialization();
int thread_initialization();


int main(int argc, char *argv[])
{
    int initResult;

    gtk_init(&argc, &argv);

    initResult = gui_initialization();

    if (initResult == 0)
        return 0;

    initResult = thread_initialization();

    if (initResult == 0)
        return 0;

    /* PID for delegated tasks */
    char * abs_simulator_arg[3] = {"./ABS_simulator/ABS_simulator", NULL};

 	/* Create fast worker process */
 	abs_simulator_pid = fork();

 	if (abs_simulator_pid == 0) 
    { 
        /* This is fast child process, just run it */
 		execvp(abs_simulator_arg[0], abs_simulator_arg);
    }
    else
    {
        inputData.reset_simulation = 0;
        mq_send(inputMQueue, (const char *)&inputData, sizeof(struct input_data_t), 0);
        gtk_main();
    }

    return 0;
}


int gui_initialization()
{
    GtkBuilder      *builder; 
    GtkWidget       *window;

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "GUI/window_main.glade", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    gtk_builder_connect_signals(builder, NULL);


    gLblVehicleVelocity = GTK_WIDGET(gtk_builder_get_object(builder, "vehicle_velocity_label"));
    gLblVehicleDistance = GTK_WIDGET(gtk_builder_get_object(builder, "vehicle_distance_label"));
    gLblSlip = GTK_WIDGET(gtk_builder_get_object(builder, "slip_label"));
    gLblFriction = GTK_WIDGET(gtk_builder_get_object(builder, "friction_label"));
    gLblAbsOnOff = GTK_WIDGET(gtk_builder_get_object(builder, "ABS_on_off_label"));
    gLblInitVehicleVelocity = GTK_WIDGET(gtk_builder_get_object(builder, "initial_vehicle_velocity_entry"));

    char buferStr[32];
    sprintf(buferStr, "%.2f", (inputData.init_vehicle_velocity * 3.6));
    gtk_entry_set_text(GTK_ENTRY(gLblInitVehicleVelocity), buferStr);

    g_object_unref(builder);

    gtk_widget_show(window);  

    return 1;
}


int thread_initialization()
{
    int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aShowDataThreadAttr);

	/* Set Message Queue size */
	outputMQueueAttr.mq_maxmsg = 2048;
	outputMQueueAttr.mq_msgsize = sizeof(struct output_data_t);

	/* Create Message Queue */
	if ((outputMQueue = mq_open("/outputMQ", O_CREAT | O_RDWR, 777, &outputMQueueAttr)) == -1) {
		fprintf(stderr, "Creation of the mqueue outputMQ failed.\n");
        return 0;
	}

    /* Set Message Queue size */
	inputMQueueAttr.mq_maxmsg = 10;
	inputMQueueAttr.mq_msgsize = sizeof(struct input_data_t);

	/* Create Message Queue */
	if ((inputMQueue = mq_open("/inputMQueue", O_CREAT | O_RDWR, 777, &inputMQueueAttr)) == -1) {
		fprintf(stderr, "Creation of the inputMQ outputMQ failed.\n");
	}

	/* Create logger thread */
	if ((status = pthread_create( &tShowDataThread, NULL, tThreadFunc, &aShowDataThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
        return 0;
	}

    return 1;
}


void *tThreadFunc(void *cookie)
{
	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_min(policy);
	pthread_setschedparam( pthread_self(), policy, &param);

    struct output_data_t dataToShow;
    char buferStr[32];

	for (;;) {
		/* Wait until something will appears in queue */
		mq_receive(outputMQueue, (char *)&dataToShow, sizeof(struct output_data_t), NULL);

        sprintf(buferStr, "%.2f", (dataToShow.vehicle_velocity * 3.6));
		gtk_label_set_text(GTK_LABEL(gLblVehicleVelocity), buferStr);

        sprintf(buferStr, "%.2f", dataToShow.vehicle_distance);
        gtk_label_set_text(GTK_LABEL(gLblVehicleDistance), buferStr);

        sprintf(buferStr, "%.2f", dataToShow.slip);
        gtk_label_set_text(GTK_LABEL(gLblSlip), buferStr);

        sprintf(buferStr, "%.2f", dataToShow.friction);
        gtk_label_set_text(GTK_LABEL(gLblFriction), buferStr);

	}
}


// called when window is closed
void on_window_main_destroy()
{
    kill(abs_simulator_pid, SIGINT);

    gtk_main_quit();

    mq_close(inputMQueue);
    mq_close(outputMQueue);

    raise(SIGINT);
}


void ABS_on_off_button_clicked_cb()
{
    static int AbsState = 1;

    if (AbsState == 0)
    {
        AbsState = 1;
        gtk_label_set_text(GTK_LABEL(gLblAbsOnOff), "ON");
        inputData.abs_on_off = AbsState;
    }
    else if (AbsState == 1)
    {
        AbsState = 0;
        gtk_label_set_text(GTK_LABEL(gLblAbsOnOff), "OFF");
        inputData.abs_on_off = AbsState;
    }
}


void start_reset_simulation_button_clicked_cb()
{
    inputData.reset_simulation = 1;

    mq_send(inputMQueue, (const char *)&inputData, sizeof(struct input_data_t), 0);

    inputData.reset_simulation = 0;
}


void generate_plots_button_clicked_cb()
{
    pid_t plot_python_pid;

    char * plot_python_arg[3] = {"./plot.py", NULL};

     	/* Create fast worker process */
 	plot_python_pid = fork();

 	if (plot_python_pid == 0) 
    { 
 		execvp(plot_python_arg[0], plot_python_arg);
    }
}


void initial_vehicle_velocity_entry_activate_cb()
{
    const char * buferStr = gtk_entry_get_text(GTK_ENTRY(gLblInitVehicleVelocity));

    inputData.init_vehicle_velocity = (strtof(buferStr, NULL)) / 3.6;
}

