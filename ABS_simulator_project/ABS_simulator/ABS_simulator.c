#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>

#include "calculation.h"
#include "output.h"
#include "input.h"
#include "logger.h"
#include "periodic.h"

static void sig_handler(int);

/*
 * Main project function
 */
int main(int argc, char *argv[]) {

	// Prepare SIGACTION struct
    struct sigaction act;
    act.sa_handler = sig_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    // Register signal handler for SIGINT
    if (sigaction(SIGINT, &act, NULL) < 0) {
       fprintf(stderr, "Cannot register SIGINT handler.\n");
        return 0;
    }

	/* Char that we get from keyboard*/
    char c;

    /* Init output thread */
    init_output();
    /* Init output thread */
	init_input();
	/* Init logger thread */
	init_logger();
	/* Start periodic task */
	init_periodic();

	/* Get the char from keyboard and send it to keyboard thread */
	while(c = getc(stdin)) {

		if (c == 'q') {
			break;
		} else if (c == '\n') {
			continue;
		} 
	}

  	/* Close logger thread */
	finalize_logger();

	/* Close output thread */
	finalize_output();

	return EXIT_SUCCESS;
}


// Signal handler
void sig_handler(int sig) {

	/* Close logger thread */
	finalize_logger();

	/* Close input thread */
	finalize_input();

	/* Close output thread */
	finalize_output();

    exit(0);
}
