#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#include "calculation.h"
#include "periodic.h"

/* Parameters values */
struct vehicle_params_t vehicle_params = {
	.mass = 1200.0, 
	.velocity_init = 0.0, 
	.velocity_min = 0.0,
	.velocity_max = 60.0,
	.distance_init = 0.0
};

struct wheel_params_t wheel_params = {
	.radius = 0.28,
	.inertia = 100,
	.angular_velocity_init = 0.0,  	// initialize in initialize_calculation() function
	.angular_velocity_min = 0.0,	// initialize in initialize_calculation() function
	.angular_velocity_max = 0.0		// initialize in initialize_calculation() function
};

struct friction_params_t friction_params_set[4] = {
	{.A = 0.9, .B = 1.07, .C = 0.2773, 	.D = 0.0026	},	// dry concrete
	{.A = 0.7, .B = 1.07, .C = 0.5, 	.D = 0.003	},	// wet concrete
	{.A = 0.3, .B = 1.07, .C = 0.1773, 	.D = 0.006	},	// snow
	{.A = 0.1, .B = 1.07, .C = 0.38, 	.D = 0.007	}	// ice
};

int friction_params_no = 0;
struct friction_params_t friction_params;

struct controller_params_t controller_params = {
	.slip_set_point = 0.2,
	.vehicle_velocity_min = 1.4,
	.brake_K = 1000.0,
	.brake_T = 0.01,
	.brake_torque_max = 2000.0
};

/* Input and outputs vars*/
struct output_data_t output_data;

double vehicle_velocity_prev;
double vehicle_distance_prev;

double wheel_angular_velocity_prev;

double controller_output_prev;
double brake_pressure_prev;

double force;
double brake_torque;

sig_atomic_t flag_ABS_state = 1;
double g_vehicle_velocity_init = 28.0;
int ABS_state;


/* Semaphores for input and output */
pthread_mutex_t input_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t output_mutex = PTHREAD_MUTEX_INITIALIZER;


/**
 * Function initializes plant module
 */
int initialize_calculation() {
	friction_params.A = friction_params_set[friction_params_no].A;
	friction_params.B = friction_params_set[friction_params_no].B;
	friction_params.C = friction_params_set[friction_params_no].C;
	friction_params.D = friction_params_set[friction_params_no].D;

	wheel_params.angular_velocity_min = vehicle_params.velocity_min / wheel_params.radius;
	wheel_params.angular_velocity_max = vehicle_params.velocity_max / wheel_params.radius;

	pthread_mutex_lock(&input_mutex);
	vehicle_params.velocity_init = g_vehicle_velocity_init;
	pthread_mutex_unlock(&input_mutex);

	vehicle_velocity_prev = vehicle_params.velocity_init;
	vehicle_distance_prev = vehicle_params.distance_init;

	wheel_params.angular_velocity_init = vehicle_params.velocity_init / wheel_params.radius;
	wheel_angular_velocity_prev = wheel_params.angular_velocity_init;

	controller_output_prev = 0.0;
	brake_pressure_prev = 0.0;

	force = 0.0;
	brake_torque = 0.0;

	ABS_state = (int)flag_ABS_state;

	return 0;
}


/**
 * Function calculates one plant step
 */
int calculate() 
{	
	/**
	 * Calculate vehicle
	 */

	/* Calculate acceleration */
	double vehicle_acceleration;
	vehicle_acceleration = (-1) * force / vehicle_params.mass;

	/* Calculate velocity */
	double vehicle_velocity_temp;
	vehicle_velocity_temp = vehicle_velocity_prev + (vehicle_acceleration * MODEL_SAMPLE_TIME);

	vehicle_velocity_prev = vehicle_velocity_temp;
	
	/* Velocity saturation */
	if (vehicle_velocity_temp < vehicle_params.velocity_min)
		vehicle_velocity_temp = vehicle_params.velocity_min;
	else if (vehicle_velocity_temp > vehicle_params.velocity_max)
		vehicle_velocity_temp = vehicle_params.velocity_max;

	double vehicle_angular_velocity_temp;
	vehicle_angular_velocity_temp = vehicle_velocity_temp / wheel_params.radius;

	double distance_temp;
	distance_temp = vehicle_distance_prev + (vehicle_velocity_temp * MODEL_SAMPLE_TIME);

	vehicle_distance_prev = distance_temp;
	

	/**
	 * Calculate wheel
	 */

	double tire_torque;
	tire_torque = force * wheel_params.radius;

	double torque_resultant;
	torque_resultant = tire_torque - brake_torque;

	double angular_acceleration;
	angular_acceleration = torque_resultant / wheel_params.inertia;

	double wheel_angular_velocity_temp;
	wheel_angular_velocity_temp = wheel_angular_velocity_prev + (angular_acceleration * MODEL_SAMPLE_TIME);

	wheel_angular_velocity_prev = wheel_angular_velocity_temp;

	/* Angular velocity saturation */
	if (wheel_angular_velocity_temp < wheel_params.angular_velocity_min)
		wheel_angular_velocity_temp = wheel_params.angular_velocity_min;
	else if (wheel_angular_velocity_temp > wheel_params.angular_velocity_max)
		wheel_angular_velocity_temp = wheel_params.angular_velocity_max;


	/**
	 * Calculate slip
	 */

	if (vehicle_angular_velocity_temp == 0)
		vehicle_angular_velocity_temp = EPS;
	
	double slip;
	slip = 1.0 - (wheel_angular_velocity_temp / vehicle_angular_velocity_temp);

	/* Slip saturation */
	if (slip < 0.0)
		slip = 0.0;
	else if (slip > 1.0)
		slip = 1.0;


	/**
	 * Calculate force
	 */

	/* Friction */
	double friction;
	slip *= 100.0;
	friction = friction_params.A * ((friction_params.B * (1 - exp((-1) * friction_params.C * slip))) - (friction_params.D * slip));
	slip /= 100.0;

	/* Friction saturation */
	if (friction < 0.0)
		friction = 0.0;

	force = friction * g * vehicle_params.mass / 4;


	/**
	 * Calculate controller
	 */

	double slip_error; 
	if ((vehicle_velocity_temp >= controller_params.vehicle_velocity_min) && (ABS_state == 1))
		slip_error = controller_params.slip_set_point - slip;
	else
		slip_error = controller_params.slip_set_point - 0.0;

	double controller_output;
	/* Bang-bang controller */
	if (slip_error > 0.0)
		controller_output = 1.0;
	else if (slip_error < 0.0)
		controller_output = -1.0;
	else
		controller_output = 0.0;
	
	double brake_pressure;
	brake_pressure = (controller_params.brake_K * (1 - exp((-1) * MODEL_SAMPLE_TIME / controller_params.brake_T)) * controller_output_prev) + (exp((-1) * MODEL_SAMPLE_TIME / controller_params.brake_T) * brake_pressure_prev);

	controller_output_prev = controller_output;
	brake_pressure_prev = brake_pressure;

	brake_torque = brake_torque + (brake_pressure * MODEL_SAMPLE_TIME);

	if (brake_torque > controller_params.brake_torque_max)
		brake_torque = controller_params.brake_torque_max;

	// printf("brake_torque: %.4f\n", brake_torque);

	pthread_mutex_lock(&output_mutex);
	output_data.vehicle_velocity = vehicle_velocity_temp;
	output_data.vehicle_angular_velocity = vehicle_angular_velocity_temp;
	output_data.vehicle_distance = distance_temp;
	output_data.wheel_angular_velocity = wheel_angular_velocity_temp;
	output_data.force = force;
	output_data.slip = slip;
	output_data.friction = friction;
	output_data.brake_torque = brake_torque;
	pthread_mutex_unlock(&output_mutex);

	if (vehicle_velocity_temp <= vehicle_params.velocity_min)
	{
		flag_run_simulation = 0;
	}

}
