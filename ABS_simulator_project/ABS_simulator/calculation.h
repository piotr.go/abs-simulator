#ifndef CALCULATION_H_
#define CALCULATION_H_

#include <signal.h>

/* Parameters struct */
struct vehicle_params_t {
	double mass; 					// [kg]
	double velocity_init;  			// [m/s]
	double velocity_min;			// [m/s]
	double velocity_max;			// [m/s]
	double distance_init;			// [m]
};

struct wheel_params_t {
	double radius;					// [m]
	double inertia;					// [kg*m^2]
	double angular_velocity_init;	// [rad/s]
	double angular_velocity_min;	// [rad/s]
	double angular_velocity_max;	// [rad/s]
};

struct friction_params_t {
	double A;
	double B;
	double C;
	double D;
};

struct controller_params_t {
	double slip_set_point;
	double vehicle_velocity_min;	// [m/s]
	double brake_K;
	double brake_T;
	double brake_torque_max;		// [N*m]
};

struct output_data_t {
	double vehicle_velocity;

	double vehicle_angular_velocity;
	double vehicle_distance;

	double wheel_angular_velocity;

	double force;

	double slip;
	double friction;

	double brake_torque;
};


/* Constants */
#define MODEL_SAMPLE_TIME (CLOCK_INTERVAL_NS / 1000000000.0)	// [s]
#define EPS 2.2204e-16
#define g 9.81

/* Make this vars public */
extern struct output_data_t output_data;

/* Make semaphores for my vars public */
extern pthread_mutex_t input_mutex;
extern pthread_mutex_t output_mutex;

extern sig_atomic_t flag_ABS_state;
extern double g_vehicle_velocity_init;


/* Initialization function */
int initialize_calculation(void);
/* One step calculation function */
int calculate(void);

#endif /* CALCULATION_H_ */
