#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include "calculation.h"
#include "input.h"
#include "periodic.h"
#include "logger.h"

/* Input thread function prototype */
void *tInputThreadFunc(void *);

/* Thread variable */
pthread_t tInputThread;
/* Thread attributes structure */
pthread_attr_t aInputThreadAttr;
/* Message Queue for sign */
mqd_t	inputMQueue;
struct	mq_attr inputMQueueAttr;

int input_debug_mode = 0;

/**
 * Input thread initialization fucntion
 */
int init_input() {

	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aInputThreadAttr);

	/* Start thread */
	if ((status = pthread_create(&tInputThread, NULL, tInputThreadFunc, &aInputThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	/* Set Message Queue size */
	inputMQueueAttr.mq_maxmsg = 10;
	inputMQueueAttr.mq_msgsize = sizeof(struct input_data_t);

	/* Create Message Queue */
	if ((inputMQueue = mq_open("/inputMQueue", O_CREAT | O_RDWR, 777, &inputMQueueAttr)) == -1) {
		fprintf(stderr, "Creation of the inputMQueue outputMQ failed.\n");
	}

	return 0;
}


/**
 *  Input thread function
 */
void *tInputThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy);
	pthread_setschedparam(pthread_self(), policy, &param);

	struct input_data_t input_data;

	/* Receive, identify and replay to message*/
	for (;;) {
		/* Wait from pulse from input */
		mq_receive(inputMQueue, (char *)&input_data, sizeof(struct input_data_t), NULL);

		if (input_debug_mode)
        {
			printf("Interrupt got message.\n");
		}

		flag_run_simulation = 0;
		
		pthread_mutex_lock(&input_mutex);
		g_vehicle_velocity_init = input_data.init_vehicle_velocity;
		pthread_mutex_unlock(&input_mutex);

		flag_ABS_state = input_data.abs_on_off;

		init_logger();
		initialize_calculation();

		flag_run_simulation = input_data.reset_simulation;
	}

	return 0;
}

/**
 * Finalize output
 */
int finalize_input() {

	/* Close Message Queue */
	mq_close(inputMQueue);

	return 0;
}
