#ifndef INPUT_H_
#define INPUT_H_


struct input_data_t {
	int abs_on_off;
	double init_vehicle_velocity;
	int reset_simulation;
};


/* Make input message queue public */
extern mqd_t inputMQ;

/* Function initialize input thread */
int init_input();
int finalize_input();

#endif /* INPUT_H_ */
