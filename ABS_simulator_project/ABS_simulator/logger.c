#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include "logger.h"
#include "calculation.h"

/* Logger thread prototype */
void *tLoggerThreadFunc(void *);

/* Mqueue variable */
mqd_t loggerMQueue;
/* Mqueue attributes structure */
struct	mq_attr loggerMQueueAttr;

/* Thread variable */
pthread_t tLoggerThread;
/* Thread attributes structure */
pthread_attr_t aLoggerThreadAttr;


/**
 * Function crates logger thread
 */
int init_logger() {

	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aLoggerThreadAttr);

	/* Set Message Queue size */
	loggerMQueueAttr.mq_maxmsg = 2048;
	loggerMQueueAttr.mq_msgsize = sizeof(struct output_data_t);

	/* Create Message Queue */
	if ((loggerMQueue = mq_open("/loggerMQ", O_CREAT | O_RDWR, 777, &loggerMQueueAttr)) == -1) {
		fprintf(stderr, "Creation of the mqueue myloggerMQ failed.\n");
		return 0;
	}

	/* Create logger thread */
	if ((status = pthread_create( &tLoggerThread, NULL, tLoggerThreadFunc, &aLoggerThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	return 0;
}

/**
 * Finalize logger
 */
int finalize_logger() {

	/* Close Message Queue */
	mq_close(loggerMQueue);

	return 0;
}

/**
 * Logger thread function
 */
void *tLoggerThreadFunc(void *cookie) {

	/* Logger file descriptor */
	FILE * logger_file;
	struct output_data_t data_to_log;

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_min(policy);
	pthread_setschedparam( pthread_self(), policy, &param);

	/* Set the fiel header */
	logger_file = fopen("sim_result.txt", "w");
	fprintf(logger_file, "ABS simulator result\n");
	fclose(logger_file);

	for (;;) {
		/* Wait until something will appears in queue */
		mq_receive(loggerMQueue, (char *)&data_to_log, sizeof(struct output_data_t), NULL);

		/* Append file with new data */
		logger_file = fopen("sim_result.txt", "a");
		fprintf(logger_file, "%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n", data_to_log.vehicle_velocity, data_to_log.vehicle_angular_velocity, data_to_log.vehicle_distance, data_to_log.wheel_angular_velocity, data_to_log.force, data_to_log.slip, data_to_log.friction, data_to_log.brake_torque);
		fclose(logger_file);

	}

	return 0;
}
