#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <mqueue.h>

#include "logger.h"
#include "calculation.h"
#include "output.h"
#include "periodic.h"

/* Thread function prototype */
void *tPeriodicThreadFunc(void *);
/* Thread counter */
int i = 0;

/* Mqueue variable */
mqd_t showDataMQueue;

sig_atomic_t flag_run_simulation = 1;


/**
 * Function starts periodic thread
 */
int init_periodic() {

	int status;

	/* Thread attributes variable */
	pthread_attr_t aPeriodicThreadAttr;
	/* Structure with time values */
	struct	itimerspec timerSpecStruct;
	/* Timer variable */
	timer_t	timerVar;
	/* Signal variable */
	struct	sigevent timerEvent;

	/* Initialize thread attributes structure */
	pthread_attr_init(&aPeriodicThreadAttr);

	/* Initialize event to create thread */
	timerEvent.sigev_notify = SIGEV_THREAD;
    timerEvent.sigev_notify_function = tPeriodicThreadFunc;
	timerEvent.sigev_notify_attributes = &aPeriodicThreadAttr;

	/* Create timer */
  	if ((status = timer_create(CLOCK_REALTIME, &timerEvent, &timerVar))) {
  		fprintf(stderr, "Error creating timer : %d\n", status);
  		return 0;
  	}

	/* Set up timer structure with time parameters */
	timerSpecStruct.it_value.tv_sec = 1;
	timerSpecStruct.it_value.tv_nsec = 0;
	timerSpecStruct.it_interval.tv_sec = 0;
	timerSpecStruct.it_interval.tv_nsec = CLOCK_INTERVAL_NS;

	/* Initialize calculation */
	initialize_calculation();

	/* Change timer parameters and run */
	timer_settime( timerVar, 0, &timerSpecStruct, NULL);

	return 0;
}

/*
 *  Periodic thread function
 */
void *tPeriodicThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy);
	pthread_setschedparam( pthread_self(), policy, &param);

	if (flag_run_simulation)
	{
		/* Calculate ABS step */
		calculate();

		/* Send the plant output to the logger */
		pthread_mutex_lock(&output_mutex);
		mq_send(loggerMQueue, (const char *)&output_data, sizeof(struct output_data_t), 0);
		pthread_mutex_unlock(&output_mutex);
	}

	/* Wake up screen thread, it knows what to do */
	i++;
	if (!(i % OUTPUT_CLOCK_DIVIDER)) 
	{
		i = 0;
		pthread_barrier_wait(&output_barrier);
	}

	return 0;
}
