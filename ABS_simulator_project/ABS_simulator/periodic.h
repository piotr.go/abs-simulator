#ifndef PERIODIC_H_
#define PERIODIC_H_

#include <signal.h>

extern sig_atomic_t flag_run_simulation;

/* Constatns */
#define CLOCK_INTERVAL_NS 1000000  // 1 ms
#define OUTPUT_CLOCK_DIVIDER 2000 

/* Function initializes periodic thread */
int init_periodic();


#endif /* PERIODIC_H_ */
