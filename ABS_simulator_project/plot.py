#!/usr/bin/python3
import matplotlib.pyplot as plt


txt_file_name = "sim_result.txt"

vehicle_velocity = []
vehicle_angular_velocity = []
vehicle_distance = []
wheel_angular_velocity = []
force = []
slip = []
friction = []
brake_torque = []

num_of_samples = 0

with open(txt_file_name, "r") as file:
    for idx, line in enumerate(file):
        if idx == 0:
            continue
        line = line[:-1]  # remove "\n"
        data = line.split("\t")
        data = [float(value) for value in data]

        vehicle_velocity.append(data[0])
        vehicle_angular_velocity.append(data[1])
        vehicle_distance.append(data[2])
        wheel_angular_velocity.append(data[3])
        force.append(data[4])
        slip.append(data[5])
        friction.append(data[6])
        brake_torque.append(data[7])

        num_of_samples = idx

        # print(vehicle_velocity)
        # break

MODEL_SAMPLE_TIME = 0.001
time_vextor = range(0, num_of_samples)
time_vextor = [sample * MODEL_SAMPLE_TIME for sample in time_vextor]

plt.figure()
plt.subplot(2, 2, 1)
plt.plot(time_vextor, vehicle_velocity)
plt.title("vechicle velocity")
plt.xlabel("time [s]")
plt.ylabel("v [m/s]")
plt.grid()

plt.subplot(2, 2, 2)
plt.plot(time_vextor, vehicle_angular_velocity)
plt.title("vehicle angular velocity")
plt.xlabel("time [s]")
plt.ylabel("w [rad/s]")
plt.grid()

plt.subplot(2, 2, 3)
plt.plot(time_vextor, vehicle_distance)
plt.title("vehicle distance")
plt.xlabel("time [s]")
plt.ylabel("d [m]")
plt.grid()

plt.subplot(2, 2, 4)
plt.plot(time_vextor, wheel_angular_velocity)
plt.title("wheel angular velocity")
plt.xlabel("time [s]")
plt.ylabel("w [rad/s]")
plt.grid()

plt.figure()
plt.subplot(2, 2, 1)
plt.plot(time_vextor, force)
plt.title("force")
plt.xlabel("time [s]")
plt.ylabel("F [N]")
plt.grid()

plt.subplot(2, 2, 2)
plt.plot(time_vextor, slip)
plt.title("slip")
plt.xlabel("time [s]")
plt.ylabel("s")
plt.grid()

plt.subplot(2, 2, 3)
plt.plot(time_vextor, friction)
plt.title("friction")
plt.xlabel("time [s]")
plt.ylabel("f")
plt.grid()

plt.subplot(2, 2, 4)
plt.plot(time_vextor, brake_torque)
plt.title("brake torque")
plt.xlabel("time [s]")
plt.ylabel("T [Nm]")
plt.grid()

plt.show()
